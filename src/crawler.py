import sys
from urllib.parse import urlparse
from bs4 import BeautifulSoup, SoupStrainer
import requests

class Crawler:
    def main(self, argv):
        if(len(argv) < 2):
            raise Exception("No Url given")
        elif not bool(urlparse(argv[1]).netloc):
            raise Exception("Invalid url")
        else:
            max = 3
            list = []
            if(len(argv) > 2):
                max = int(argv[2])
            if(max == 1):
                print("Crawling from " + argv[1] +
                    " to a maximum distance of a " + str(max) + " link")
            else:
                print("Crawling from " + argv[1] +
                    " to a maximum distance of a " + str(max) + " links")
                 
            self.crawl(argv[1], 0, max, list)

    def crawl(self, url, depth, maxdepth, visited):
            if depth > maxdepth:
                return
            print(("\t" * depth) + url)
            newList = []
            try:
                r = requests.get(url)
                data = r.text
                soup = BeautifulSoup(data,"lxml")
                for link in soup.find_all('a'):
                    newUrl = link.get('href')# + link.get('schema')
                    parsedUrl = urlparse(newUrl)
                    if newUrl:
                        if newUrl not in visited and newUrl[0] != "#" and newUrl != "/":
                            if parsedUrl.scheme == "" and parsedUrl.netloc == "" :
                                parsedUrl2 = urlparse(url)
                                newUrl = parsedUrl2.scheme + "://" + parsedUrl2.netloc + newUrl         
                            newList.append(newUrl)
                for links in newList:
                    if links not in visited or links + "/" not in visited:
                        visited.append(links)     
                        self.crawl(links, depth + 1, maxdepth, visited)
            except:
                pass
Main = Crawler()
Main.main(sys.argv)